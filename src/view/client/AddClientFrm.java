package view.client;

import dao.ClientDAO;
import model.Client;
import model.Shift;
import model.User;
import view.Booking.ConfirmFrm;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class AddClientFrm extends JFrame implements ActionListener {
    private User user;
    private Shift shift;
    private JTextField txtClientFullName;
    private JTextField txtClientID;
    private JTextField txtPhoneNumber;
    private JTextField txtEmailAddress;
    private JTextField txtAddress;
    private JButton btnAdd;
    private JTable tblResult;
    private ArrayList<Client> listClient;
    private AddClientFrm mainFrm;
    private JButton btnFind;

    //    public AddClientFrm(User user){
    public AddClientFrm(User user, Shift shift) {
        super("AddCLientFrm");
        listClient = new ArrayList<>();
        this.user = user;
        mainFrm = this;

        JPanel pnMain = new JPanel();
        pnMain.setSize(this.getSize().width - 5, this.getSize().height - 20);
        pnMain.setLayout(new BoxLayout(pnMain, BoxLayout.Y_AXIS));
        pnMain.add(Box.createRigidArea(new Dimension(0, 10)));

        JLabel lblHome = new JLabel("Check client's information");
        lblHome.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblHome.setFont(lblHome.getFont().deriveFont(20.0f));
        pnMain.add(lblHome);
        pnMain.add(Box.createRigidArea(new Dimension(0, 20)));

        JPanel pn1 = new JPanel();
        pn1.setSize(600, 150);
        pn1.setLayout(new GridLayout(5, 2, 5, 5));
        pn1.add(new JLabel("Client identification: "));
        txtClientID = new JTextField();
        pn1.add(txtClientID);
        pn1.add(new JLabel("Client name: "));
        txtClientFullName = new JTextField();
        pn1.add(txtClientFullName);
        pn1.add(new JLabel("Client phone number: "));
        txtPhoneNumber = new JTextField();
        pn1.add(txtPhoneNumber);
        pn1.add(new JLabel("Client adress: "));
        txtAddress = new JTextField();
        pn1.add(txtAddress);
        pn1.add(new JLabel("Client email: "));
        txtEmailAddress = new JTextField();
        pn1.add(txtEmailAddress);

        pnMain.add(pn1);
        pnMain.add(Box.createRigidArea(new Dimension(0, 10)));

        JPanel pnBtn = new JPanel();
        pnBtn.setLayout(new FlowLayout(FlowLayout.RIGHT));
        btnFind = new JButton("Find");
        pnBtn.add(btnFind);
        btnFind.addActionListener(this);
        btnAdd = new JButton("Add new client");
        btnAdd.addActionListener(this);
        pnBtn.add(btnAdd);
        pnMain.add(pnBtn);

        JPanel pn2 = new JPanel();
        pn2.setLayout(new BoxLayout(pn2, BoxLayout.Y_AXIS));
        tblResult = new JTable();
        JScrollPane scrollPane = new JScrollPane(tblResult);
        tblResult.setFillsViewportHeight(false);
        scrollPane.setPreferredSize(new Dimension(scrollPane.getPreferredSize().width, 250));
        tblResult.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int column = tblResult.getColumnModel().getColumnIndexAtX(e.getX()); // get the coloum of the button
                int row = e.getY() / tblResult.getRowHeight(); // get the row of the button

                // *Checking the row or column is valid or not
                if (row < tblResult.getRowCount() && row >= 0 && column < tblResult.getColumnCount() && column >= 0) {
                    (new ConfirmFrm(user, listClient.get(row), shift)).setVisible(true);
                    mainFrm.dispose();
                }
            }
        });
        pn2.add(scrollPane);
        pnMain.add(pn2);

        this.setSize(400, 600);
        this.setLocation(200, 10);
        this.add(pnMain);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((e.getSource() instanceof JButton) && ((e.getSource()).equals(btnFind))) {
            if (txtClientFullName.getText() == null || txtClientFullName.getText().length() == 0) {
                return;
            }
            ClientDAO cd = new ClientDAO();
            Client client = new Client();
            client.setFullName(txtClientFullName.getText().trim());

            listClient = cd.findClient(client.getFullName());
            String[] columnNames = {"Id", "ServiceName", "Start hour", "Date"};
            String[][] value = new String[listClient.size()][5];
            for (int i = 0; i < listClient.size(); i++) {
                value[i][0] = listClient.get(i).getId() + "";
                value[i][1] = listClient.get(i).getFullName();
                value[i][2] = listClient.get(i).getPhoneNumber();
                value[i][3] = listClient.get(i).getEmail();
            }
            DefaultTableModel tableModel = new DefaultTableModel(value, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    //unable to edit cells
                    return false;
                }
            };
            tblResult.setModel(tableModel);
        } else if ((e.getSource() instanceof JButton) && ((e.getSource()).equals(btnAdd))) {
            if (txtClientID.getText() == null || txtClientID.getText().length() == 0 ||
                    txtEmailAddress.getText() == null || txtEmailAddress.getText().length() == 0 ||
                    txtPhoneNumber.getText() == null || txtPhoneNumber.getText().length() == 0 ||
                    txtClientFullName.getText() == null || txtClientFullName.getText().length() == 0 ||
                    txtAddress.getText() == null || txtAddress.getText().length() == 0) {
                return;
            }
            ClientDAO cd = new ClientDAO();
            Client client = new Client();
            client.setPhoneNumber(txtPhoneNumber.getText().trim());
            client.setEmail(txtEmailAddress.getText().trim());
            client.setFullName(txtClientFullName.getText().trim());
            client.setId(Integer.parseInt(txtClientID.getText().trim()));
            if(cd.addClient(client)){
                JOptionPane.showMessageDialog(this, "Them thanh cong");
            }
            else{
                JOptionPane.showMessageDialog(this, "them that bai");
            }
        } else {
            JOptionPane.showMessageDialog(this, "!!!!!");
        }
    }
}
