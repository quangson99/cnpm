package view.Booking;

import dao.BookingDAO;
import model.Booking;
import model.Client;
import model.Shift;
import model.User;
import view.user.ManageHomeFrm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ConfirmFrm extends JFrame implements ActionListener {
    private Client client;
    private User user;
    private Shift shift;
    private JButton btnSave;
    private LocalDate date = LocalDate.now();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    //    public ConfirmFrm(User user) {
    public ConfirmFrm(User user, Client client, Shift shift) {
        super("ComfirmFrm");
        this.client = client;
        this.shift = shift;
        this.user = user;

        JPanel pnMain = new JPanel();
        pnMain.setLayout(new BoxLayout(pnMain, BoxLayout.Y_AXIS));
        pnMain.add(Box.createRigidArea(new Dimension(0, 10)));

        JLabel lblHome = new JLabel("Confirm");
        lblHome.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblHome.setFont(lblHome.getFont().deriveFont(20.0f));
        pnMain.add(lblHome, BorderLayout.NORTH);
        pnMain.add(Box.createRigidArea(new Dimension(0, 20)));

        JPanel pnConfirm = new JPanel();
        pnConfirm.setLayout(new GridLayout(6, 1, 10, 10));
        JLabel lblcmt = new JLabel("Chung minh thu: " + client.getId());
        pnConfirm.add(lblcmt);
        JLabel lblName = new JLabel("Ho ten: " + client.getFullName());
        pnConfirm.add(lblName);
        JLabel lblPhone = new JLabel("So dien thoai: " + client.getPhoneNumber());
        pnConfirm.add(lblPhone);
        JLabel lblServiceName = new JLabel("Ten dich vu: " + shift.getService().getName());
        pnConfirm.add(lblServiceName);
        JLabel lblStartHour = new JLabel("Gio bat dau: " + shift.getStartHour());
        pnConfirm.add(lblStartHour);
        JLabel lblDate = new JLabel("Ngay: " + shift.getDate());
        pnConfirm.add(lblDate);
        pnMain.add(pnConfirm);
        pnMain.add(Box.createRigidArea(new Dimension(0, 10)));


        JPanel pn = new JPanel();
        pn.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JLabel lblOrderDay = new JLabel("Ngay dat lich: "+date.format(formatter));
        pn.add(lblOrderDay);

        pnMain.add(pn);
//        pnMain.add(Box.createRigidArea(new Dimension(0,10)));

        JPanel pnButton = new JPanel();
        pnButton.setLayout(new FlowLayout(FlowLayout.RIGHT));
        btnSave = new JButton("Save");
        pnButton.add(btnSave);
        btnSave.addActionListener(this);
        pnMain.add(pnButton);

        this.setSize(300, 400);
        this.setLocation(200, 10);
        this.add(pnMain, BorderLayout.CENTER);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((e.getSource() instanceof JButton) && ((e.getSource()).equals(btnSave))) {
            Booking booking = new Booking();
            booking.setClient(client);
            booking.setShift(shift);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            booking.setNgayDatLich(date.format(formatter));

            BookingDAO bd = new BookingDAO();
            if (bd.save(booking, user)) {
                (new ManageHomeFrm(user)).setVisible(true);
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Error!");
            }
        }
    }
}
