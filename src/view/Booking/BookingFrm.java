package view.Booking;

import dao.ShiftDAO;
import model.Shift;
import model.User;
import view.client.AddClientFrm;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class BookingFrm extends JFrame implements ActionListener {
    private User user;
    private ArrayList<Shift> listShift;
    private JTextField txtNgay;
    private JTextField txtThang;
    private JTextField txtNam;
    private JTextField txtService;
    private JTable tblResult;
    private JButton btnFind;
    private JTextField txtStartHour;
    private BookingFrm mainFrm;

    public BookingFrm(User user) {
        super("BookingFrm");
        mainFrm = this;
        listShift = new ArrayList<>();
        this.user = user;

        JPanel pnMain = new JPanel();
        pnMain.setSize(this.getSize().width - 5, this.getSize().height - 20);
        pnMain.setLayout(new BoxLayout(pnMain, BoxLayout.Y_AXIS));
        pnMain.add(Box.createRigidArea(new Dimension(0, 10)));

        JLabel lblHome = new JLabel("Search a room to edit");
        lblHome.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblHome.setFont(lblHome.getFont().deriveFont(20.0f));
        pnMain.add(lblHome);
        pnMain.add(Box.createRigidArea(new Dimension(0, 20)));

        JPanel pn1 = new JPanel();
        pn1.setLayout(new BoxLayout(pn1, BoxLayout.X_AXIS));
        pn1.setSize(this.getSize().width - 5, 20);
        pn1.add(new JLabel("Gio: "));
        txtStartHour = new JTextField();
        pn1.add(txtStartHour);
        pn1.add(new JLabel("Ngay: "));
        txtNgay = new JTextField();
        pn1.add(txtNgay);
        pn1.add(new JLabel("Thang: "));
        txtThang = new JTextField();
        pn1.add(txtThang);
        pn1.add(new JLabel("Nam: "));
        txtNam = new JTextField();
        pn1.add(txtNam);
        pn1.add(new JLabel("Dich Vu: "));
        txtService = new JTextField();
        pn1.add(txtService);
        pn1.add(Box.createRigidArea(new Dimension(10, 0)));
        btnFind = new JButton("Search");
        btnFind.addActionListener(this);
        pn1.add(btnFind);
        pnMain.add(pn1);
        pnMain.add(Box.createRigidArea(new Dimension(0, 10)));

        JPanel pn2 = new JPanel();
        pn2.setLayout(new BoxLayout(pn2, BoxLayout.Y_AXIS));
        tblResult = new JTable();
        JScrollPane scrollPane = new JScrollPane(tblResult);
        tblResult.setFillsViewportHeight(false);
        scrollPane.setPreferredSize(new Dimension(scrollPane.getPreferredSize().width, 250));
        tblResult.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int column = tblResult.getColumnModel().getColumnIndexAtX(e.getX()); // get the coloum of the button
                int row = e.getY() / tblResult.getRowHeight(); // get the row of the button

                // *Checking the row or column is valid or not
                if (row < tblResult.getRowCount() && row >= 0 && column < tblResult.getColumnCount() && column >= 0) {
                    (new AddClientFrm(user, listShift.get(row))).setVisible(true);
                    mainFrm.dispose();
                }
            }
        });


        pn2.add(scrollPane);
        pnMain.add(pn2);
        this.setSize(600, 300);
        this.setLocation(200, 10);
        this.add(pnMain);
//        this.add(pn2);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((e.getSource() instanceof JButton) && (((JButton) e.getSource()).equals(btnFind))) {
            if ((txtStartHour.getText() == null) || (txtStartHour.getText().length() == 0) ||
                    (txtNgay.getText() == null) || (txtNgay.getText().length() == 0) ||
                    (txtThang.getText() == null) || (txtThang.getText().length() == 0) ||
                    (txtNam.getText() == null) || (txtNam.getText().length() == 0) ||
                    (txtService.getText() == null) || (txtService.getText().length() == 0)) {
                return;
            }
            ShiftDAO sd = new ShiftDAO();
            Shift shift = new Shift();
            shift.setService(txtService.getText().trim());
            if(txtNgay.getText().length()==1){
                String s = "0"+txtNgay.getText();
                txtNgay.setText(s);
            }
            if(txtThang.getText().length()==1){
                String s = "0"+txtThang.getText();
                txtThang.setText(s);
            }
            String date = txtNam.getText().trim() + "-" + txtThang.getText().trim() + "-" + txtNgay.getText().trim();
            shift.setDate(date);
            shift.setStartHour(Integer.parseInt(txtStartHour.getText().trim()));
            listShift = sd.findShift(shift);

            String[] columnNames = {"Id", "ServiceName", "Start hour", "Date"};
            String[][] value = new String[listShift.size()][4];
            for (int i = 0; i < listShift.size(); i++) {
                value[i][0] = listShift.get(i).getId() + "";
                value[i][1] = listShift.get(i).getService().getName();
                value[i][2] = listShift.get(i).getStartHour() + "";
                value[i][3] = listShift.get(i).getDate();
            }
            DefaultTableModel tableModel = new DefaultTableModel(value, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    //unable to edit cells
                    return false;
                }
            };
            tblResult.setModel(tableModel);
        }
    }
}
