package test.frm;

import model.Service;
import model.Shift;
import model.User;
import view.client.AddClientFrm;

public class AddClientFrmTest {
    public static void main(String[] args) {
        User u = new User(1,"Son", "ad","ad","quanly");
        Service service = new Service(1, "nhuom toc", 1000);
        Shift shift = new Shift(1,2,14,"04-07-2020","a",service,true);
        AddClientFrm a = new AddClientFrm(u, shift);
        a.setVisible(true);
    }
}
