package test.DAO;

import dao.ClientDAO;
import model.Client;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.util.ArrayList;

public class ClientDAOTest {
    ClientDAO cd = new ClientDAO();

    @Test
    public void testFindClientException1(){
        String name = "xxxxxxxxxx";
        ArrayList<Client> listClient = cd.findClient(name);
        Assert.assertNotNull(listClient);
        Assert.assertEquals(0, listClient.size());
        return;
    }

    @Test
    public void testFindClientException2(){
        String name = "ha";
        ArrayList<Client> listClient = cd.findClient(name);
        Assert.assertNotNull(listClient);
        Assert.assertEquals(0, listClient.size());
        return;
    }

    @Test
    public void testFindClientException3(){
        String key = "son";
        ArrayList<Client> listClient = cd.findClient(key);
        Assert.assertNotNull(listClient);
        Assert.assertEquals(2, listClient.size());
        return;
    }

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(ClientDAOTest.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }
}
