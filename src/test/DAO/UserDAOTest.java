package test.DAO;

import dao.UserDAO;
import model.Service;
import model.Shift;
import model.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class UserDAOTest {
    UserDAO ud = new UserDAO();

    @Test
    public void testFindClientException1(){
        User u = new User(1,"Son", "admin","admin","quanly");
        Assert.assertEquals(true, ud.checkLogin(u));
        return;
    }

    @Test
    public void testFindClientException2(){
        User u = new User(1,"Son", "xxx","xxx","quanly");
        Assert.assertEquals(false, ud.checkLogin(u));
        return;
    }
}
