package test.DAO;

import dao.ShiftDAO;
import model.Client;
import model.Service;
import model.Shift;
import model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.util.ArrayList;

public class ShiftDAOTest {
    ShiftDAO sd = new ShiftDAO();

    @Test
    public void testFindClientException1(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "nhuom toc", 1000);
        Shift shift = new Shift(1,2,14,"04-07-2020","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());
        return;
    }

    @Test
    public void testFindClientException4(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "nhuom toc", 1000);
        Shift shift = new Shift(1,2,25,"04-07-2020","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(0, list.size());
        return;
    }

    @Test
    public void testFindClientException6(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "nhuom toc", 1000);
        Shift shift = new Shift(1,2,25,"05-07-2020","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(0, list.size());
        return;
    }

    @Test
    public void testFindClientException7(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "nhuom toc", 1000);
        Shift shift = new Shift(1,2,14,"xxxxx","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(0, list.size());
        return;
    }

    @Test
    public void testFindClientException2(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "xxxxx", 1000);
        Shift shift = new Shift(1,2,14,"04-07-2020","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(0, list.size());
        return;
    }

    @Test
    public void testFindClientException3(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "tam trang", 1000);
        Shift shift = new Shift(1,2,7,"04-07-2020","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());
        return;
    }



    @Test
    public void testFindClientException5(){
//        User u = new User(1,"Son", "ad","ad","quanly");
//        Client client = new Client(1,"Nam", "abc@gmail.com", "123456", "ha noi");
        Service service = new Service(1, "cat toc", 1000);
        Shift shift = new Shift(1,2,25,"04-07-2020","a",service,true);
        ArrayList<Shift> list = sd.findShift(shift);
        Assert.assertNotNull(list);
        Assert.assertEquals(0, list.size());
        return;
    }

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(ShiftDAOTest.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }
}
