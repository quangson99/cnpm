package dao;

import model.Booking;
import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BookingDAO extends DAO {
    public BookingDAO() {
        super();
    }

    public boolean save(Booking booking, User user) {
        boolean result = false;
        String sql1= "insert into Lich(KhachHangID, NguoiDungID, CaPhucVuID, NgayDat, DaXacNhan)" +
                "values(?, ?, ?, ?, ?);";
        String sql2 = "update CaPhucVu set TrangThai = ? " +
                "where CaPhucVuID = ?;";
        try {
            PreparedStatement ps = con.prepareStatement(sql1);
            ps.setInt(1, booking.getClient().getId());
            ps.setInt(2, user.getId());
            ps.setInt(3, booking.getShift().getId());
            ps.setString(4, booking.getNgayDatLich());
            ps.setInt(5, 1);
            ps.executeUpdate();

            ps = con.prepareStatement(sql2);
            ps.setBoolean(1, false);
            ps.setInt(2, booking.getShift().getId());
            ps.executeUpdate();
//            ResultSet rs = ps.executeQuery();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
