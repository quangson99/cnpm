package dao;

import model.Client;
import model.Shift;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClientDAO  extends DAO{
    public ClientDAO() {
        super();
    }

    public ArrayList<Client> findClient(String clientName){
        ArrayList<Client> result = new ArrayList<>();
        String sql = "SELECT * FROM KhachHang WHERE HoTen = ?";
        try{
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, clientName);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Client c = new Client();
                c.setId(rs.getInt("KhachHangID"));
                c.setFullName(rs.getString("HoTen"));
                c.setEmail(rs.getString("Email"));
                c.setPhoneNumber(rs.getString("SoDienThoai"));
                result.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addClient(Client client){
        boolean result = false;
        String sql = "Insert into KhachHang(KhachHangID, HoTen, DiaChi, SoDienThoai, Email)" +
                "values(?, ?, ?, ?, ?);";
        try{
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, client.getId());
            ps.setString(2, client.getFullName());
            ps.setString(3, client.getAddress());
            ps.setString(4, client.getPhoneNumber());
            ps.setString(5, client.getEmail());
            ps.executeUpdate();
            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
