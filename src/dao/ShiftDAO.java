package dao;

import model.Shift;
import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ShiftDAO extends DAO {

    public ShiftDAO() {
        super();
    }

    public ArrayList<Shift> findShift(Shift shift) {
        ArrayList<Shift> result = new ArrayList<>();
        String sql = "select CaPhucVuID, Ngay, GioBatDau, Ten from CaPhucVu join DichVu on CaPhucVu.DichVuID = DichVu.DichVuID " +
                "where GioBatDau = ? AND Ngay = ? AND Ten = ? AND TrangThai = true;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, shift.getStartHour());
            ps.setString(2, shift.getDate());
            ps.setString(3, shift.getService().getName());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Shift rm = new Shift();
                rm.setId(rs.getInt("CaPhucVuID"));
                rm.setService(rs.getString("Ten"));
                rm.setTrangThai(true);
                rm.setDate(rs.getString("Ngay"));
                rm.setStartHour(Integer.parseInt(rs.getString("GioBatDau")));
                result.add(rm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
