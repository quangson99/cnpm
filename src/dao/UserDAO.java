package dao;

import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAO extends DAO {
    public UserDAO() {
        super();
    }

    public boolean checkLogin(User user) {
        boolean result = false;
        String sql = "SELECT NguoiDungID, Ten, ViTri FROM NguoiDung WHERE TenDangNhap = ? AND MatKhau = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user.setId(rs.getInt("NguoiDungID"));
                user.setFullName(rs.getString("Ten"));
                user.setPosition(rs.getString("ViTri"));
                result = true;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
