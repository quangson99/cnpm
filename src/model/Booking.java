package model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

public class Booking implements Serializable {
    private String ngayDatLich;
    private Client client;
    private Shift shift;

    public Booking() {
    }

    public Booking(String ngayDatLich, Client client, Shift shift) {
        this.ngayDatLich = ngayDatLich;
        this.client = client;
        this.shift = shift;
    }

    public String getNgayDatLich() {
        return ngayDatLich;
    }

    public void setNgayDatLich(String ngayDatLich) {
        this.ngayDatLich = ngayDatLich;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }
}
