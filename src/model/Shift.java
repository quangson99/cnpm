package model;

import java.io.Serializable;

public class Shift implements Serializable {
    private int id;
    private int duration;
    private int startHour;
    private String date;
    private String nhanVienPhucVu;
    private Service service;
    private boolean trangThai;

    public Shift() {
    }

    public Shift(int id, int duration, int startHour, String date, String nhanVienPhucVu, Service service, boolean trangThai) {
        this.id = id;
        this.duration = duration;
        this.startHour = startHour;
        this.date = date;
        this.nhanVienPhucVu = nhanVienPhucVu;
        this.service = service;
        this.trangThai = trangThai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getNhanVienPhucVu() {
        return nhanVienPhucVu;
    }

    public void setNhanVienPhucVu(String nhanVienPhucVu) {
        this.nhanVienPhucVu = nhanVienPhucVu;
    }

    public Service getService() {
        return service;
    }

    public void setService(String serviceName) {
        Service service = new Service();
        service.setName(serviceName);
        this.service = service;
    }

    public boolean isTrangThai() {
        return trangThai;
    }

    public void setTrangThai(boolean trangThai) {
        this.trangThai = trangThai;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
